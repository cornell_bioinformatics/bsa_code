### What for ###
This is the code for the Biohpc workshop https://biohpc.cornell.edu/ww/1/Default.aspx?wid=124

### Dependence ###
bfctools
samtools  
jacard


### Disclaim ###
We just repeat the idea from a previous publication, and please do cite their work.

Soyk, S., Lemmon, Z.H., Oved, M., Fisher, J., Liberatore, K.L., Park, S.J., Goren, A., Jiang, K., Ramos,
A., van der Knaap, E. and Van Eck, J., 2017. Bypassing negative epistasis on yield in tomato
imposed by a domestication gene. Cell, 169(6), pp.1142-1155.

And it is also risky for you to use it if you do not know the meaning of the parameters. 
Many parameters have been imbedded in the code so make sure you understand them before you run it. Good luck for your research! 