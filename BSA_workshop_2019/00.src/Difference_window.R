library(dplyr)
args <- commandArgs() 
inp<-args[5]


t=read.table(inp,head=F,stringsAsFactors=FALSE, colClasses = c("character","integer",
 "character", "character","integer","integer","integer","integer","integer"), )
names(t)=c("chr","pos","ref","alt","depth","p1_ref","p1_alt","p2_ref","p2_alt")


#### Ratio of Ratio #########
t$R1=t$p1_alt/t$p1_ref
t$R2=t$p2_alt/t$p2_ref
t$ratio=abs(t$R1-t$R2)

## find the max position of each chr, only considering contig >1Mbp

allchr.max= aggregate(t$pos, by = list(t$chr), max)
names(allchr.max)=c("chr","max.pos")
chr.max=subset(allchr.max, allchr.max$max.pos>1000000)
print(chr.max)
################# making windows   ###########################################


wind.str <- c()
wind.end <- c()
wind.chr <- c()

for(i in 1:dim(chr.max)[1]){ 
       #print(i) 
       #print(as.character(chr.max$chr[i]))
    wind.str.perm <- seq(0, chr.max$max.pos[i]+1000000,100000)      
    wind.end.perm <-  wind.str.perm + 1000000  
    wind.str <- c(wind.str,wind.str.perm)  
    wind.end <- c(wind.end,wind.end.perm) 
    wind.chr.perm <- rep(as.character(chr.max$chr[i]),length(wind.str.perm))
    wind.chr <- c(wind.chr,wind.chr.perm)
}
wind.bin <-  data.frame(chr=wind.chr,start=wind.str,end=wind.end)
head(wind.bin)

##########  calculate for each window  #################################################
df=data.frame(t)
result<- df %>% inner_join(wind.bin, by = "chr") %>%
  filter(start  <= pos & pos < end ) %>%
  group_by(chr, start, end ) %>%
  summarise(RS = median(ratio,na.rm=TRUE)) %>%
  select(chr, start, end, RS)


t.result= as.data.frame(result)
write.table( t, file=paste0(inp,".abs_diff.txt"), quote=F,sep="\t")
write.table(t.result,file=paste0(inp,".abs_diff_window.txt"),quote=F,sep="\t")




