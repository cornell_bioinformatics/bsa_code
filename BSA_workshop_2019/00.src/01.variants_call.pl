#!/usr/local/bin/perl
use strict;
use warnings;

#prepare gatk reference
#bwa index reference.fasta 
#java -jar /programs/picard-tools-2.9.0/picard.jar CreateSequenceDictionary R=reference.fasta 
#samtools faidx reference.fasta  

my $MAPQ =  60;

my $samplefile = $ARGV[0];
my $outdir = $ARGV[1];
my $grape_genome_db = $ARGV[2]; 


open (IN, "$samplefile")  || die "Error: cannot open sample file $samplefile\n";
my @samples;
my %sample2file1;
my %sample2file2;
my $paired = 1;
LOOP1:while (<IN>) 
{
	next LOOP1 unless (/\w/);
	chomp;
	s/\s+$//;
	my ($sample, $file1, $file2) = split "\t";
	unless ($file1=~/\w/) 
	{
		next LOOP1;
	}


	if ((defined $sample) && ($sample=~/\w/) )
	{
		$sample=~s/\s//g;
		unless (exists $sample2file1{$sample}) 
		{
			push @samples, $sample;
			#print "Error: Sample name $sample is duplicated in the file $samplefile. Exit now!\n";
			#exit;
		}
		
		if (-e $file1) 
		{
			if (exists $sample2file1{$sample}) 
			{
					unless ($sample2file1{$sample}=~/$file1/) 
					{
						$sample2file1{$sample} .= "&&". $file1;
					}
			}
			else
			{
				$sample2file1{$sample} = $file1;
			}
		}
		else
		{
			print "Warning: $file1 in Sample file $samplefile does not exist! Exit now!\n";
			#exit;
		}

		if ((defined $file2)&& ($file2=~/\w/) )
		{
			if (-e $file2) 
			{
				if (exists $sample2file2{$sample})  
				{
					unless ($sample2file2{$sample}=~/$file2/) 
					{
						$sample2file2{$sample} .= "&&". $file2;
					}					
				} 
				else
				{
					$sample2file2{$sample} = $file2;
				}				
			}
			else
			{
				print "Warning $file2 in Sample file $samplefile does not exist! Exit now!\n";
				#exit;
			}
		}
		else
		{
			$paired =0;
		}		 
	}
}
close IN;



our $picard_path = "/programs/picard-tools-2.9.0";
our $fastx_trimmer_cmd = "gunzip -c xxxxx |fastx_trimmer -Q33 -f 1 -l 45 -o yyyyy.r1.fastq";
our $gatk_path = "/programs/bin/GATK/";

my $threads = 8;

if (-d $outdir) 
{
	print "$outdir exists! Please delete before run\n";
	exit;
}

mkdir $outdir;

my $bwalog = "$outdir/bwalog";

open LOG, ">$bwalog";

my $bamfiles = " ";
my $command;
LOOP1:foreach my $sample (@samples)
{
	my $file1 = $sample2file1{$sample};
	my $file2 ="";
	if ($paired==1) 
	{
		$file2 = $sample2file2{$sample};
	}
	$sample=~s/\..+//;
	###  mapping
	$command = "bwa mem -t $threads  -M -R '\@RG\\tID:$sample\\tSM:$sample' $grape_genome_db $file1 $file2  | samtools sort -@ 8 -o $outdir/${sample}.sorted.bam -  2>> $bwalog";
	print LOG $command, "\n"; 
	system ($command);

	
	$command = "java -jar ${picard_path}/picard.jar BuildBamIndex INPUT=$outdir/${sample}.sorted.bam QUIET=true VERBOSITY=ERROR";
	print LOG $command, "\n"; 
	system ($command);

	$bamfiles.=" $outdir/${sample}.sorted.bam "; 

}

$command = "samtools  mpileup -t AD,DP -C 50 -Q 20 -q 40 -f $grape_genome_db $bamfiles -v  | bcftools call --consensus-caller --variants-only --pval-threshold 1.0 -O z  -o  $outdir/Out.vcf.gz";

print LOG $command, "\n"; 
system ($command);


$command = "bcftools filter  -g10  -G10 -i \'(DP4[0]+DP4[1])>1 & (DP4[2]+DP4[3])>1 & FORMAT/DP[]>5\'  $outdir/Out.vcf.gz  |    bcftools view -m2 -M2  -  -O z -o $outdir/filter.vcf.gz";
print LOG $command, "\n"; 
system ($command);


$command = "bcftools  query   -i \'TYPE=\"SNP\"\'  -f \'%CHROM\t%POS\t%REF\t%ALT{0}\t%DP[\t%AD]\n\' $outdir/filter.vcf.gz | sed \'s/[,]/\t/g\' - > $outdir/filter.vcf.txt";
print LOG $command, "\n"; 
system ($command);


