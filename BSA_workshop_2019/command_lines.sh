cp -r /shared_data/BSA_workshop_2018/*    ./
cd 01.reference
ln -s Solanum_lycopersicum.SL2.50.dna.toplevel.fa reference.fasta
bwa index reference.fasta 
java -jar /programs/picard-tools-2.9.0/picard.jar CreateSequenceDictionary R=reference.fasta 
samtools faidx reference.fasta 
cd ../
perl 00.src/01.variants_call.pl reads_table  03.bam/  01.reference/reference.fasta
cd 03.bam
R --vanilla --slave --args  filter.vcf.txt  < ../00.src/Difference_window.R 
R --vanilla --slave --args  filter.vcf.txt.abs_diff_window.txt   < ../00.src/plot_signal.R 
R --vanilla --slave --args  filter.vcf.txt  < ../00.src/Ratio_window.R 
R --vanilla --slave --args  filter.vcf.txt.ratio_window.txt   < ../00.src/plot_signal.R 
R --vanilla --slave --args  filter.vcf.txt  < ../00.src/Fisher_window.R 
R --vanilla --slave --args  filter.vcf.txt.fisher_window.txt  < ../00.src/plot_signal.R 
